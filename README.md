---
title: "Altium GitLab CI/CD demo"
author: CieNTi
tags: [ "Altium", "GitLab CI/CD" ]
---

## Description

Here is explained how to setup both an Altium project and its Git repository (GitLab used as backend) in order to be able to create a full automated build + release flow, being human-error free as much as possible.

## Overview

Altium files are binary files, so Git is not supposed to be the best place for this kind of files: In order to keep a binary file history, any change made on that file will generate a new full copy (no incremental changes here!), which will make increase the repository faster than usual.

Here is where [Git LFS][Git-LFS] comes to the rescue:

> **An open source Git extension for versioning large files**
>
> Git Large File Storage (LFS) replaces large files such as audio samples, videos, datasets, and graphics with text pointers inside Git, while storing the file contents on a remote server like GitHub.com or GitHub Enterprise.

## Repository setup

## Prerequisites and dependencies

> Please add a good documentation here. Otherwise it will break!

## Altium project setup

> Type a nice recipe to easily get your environment ready

## Usage

> You know how to use *your thing*, now explain it to the masses


<!-- Standard section //-->

## Versioning

We use [SemVer](http://semver.org/) for versioning. Available versions can be found as [tags on this repository](-/tags). 

## Troubleshooting

> Because it will fail, try to be as verbose as possible here :D

## Credits and/or sources

> *Good practice:* Add references here. Use them along the document

- [Contact me!][CieNTi-contact]
- [Visit my site!][CieNTi-dot-com]

[CieNTi-contact]: mailto:cienti@cienti.com?subject=[altium-gitlab-cicd-demo]%20Edit%20this!
[CieNTi-dot-com]: https://www.cienti.com

<!-- Description links //-->

[Git-LFS]: https://git-lfs.github.com/

## Copyright and License

**TL;DR**

```
 README.md - Altium GitLab CI/CD demo
 Copyright (C) 2020 CieNTi <cienti@cienti.com>
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

To read the *full text* please refer to the [LICENSE file](./LICENSE)
